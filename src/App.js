import Chat from './components/Chat.js';

const App = () => {
  return (
    <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
  );
};

export default App;
