export const api = (url) => {
  return fetch(url)
    .then((res) => res.json())
    .then((result) => result);
};
