import React from 'react';

export const Avatar = ({ src }) => {
  return (
    <img className="message-user-avatar" src={src} alt="avatar" style={style} />
  );
};

const style = {
  objectFit: 'cover',
  width: '150px',
  height: '100%',
};
