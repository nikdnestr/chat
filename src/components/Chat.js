import React, { useState, useEffect } from 'react';
import { Header } from './Header';
import { MessageList } from './MessageList';
import { Form } from './Form';
import { api } from '../api/fetch';
import { Preloader } from './Preloader';
import { v4 as uuidv4 } from 'uuid';

const Chat = ({ url }) => {
  useEffect(() => {
    const fetch = async () => {
      const result = await api(url);
      setMessages(result);
      setIsLoaded(true);
    };
    fetch();
  }, [url]);

  const [messages, setMessages] = useState([]);
  const [input, setInput] = useState('');
  const [isLoaded, setIsLoaded] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    const newMessage = {
      id: uuidv4(),
      userId: uuidv4(),
      avatar:
        'https://pbs.twimg.com/profile_images/1030941554763096065/clDF9QRY.jpg',
      user: 'Nik',
      text: `${input}`,
      createdAt: Date.now(),
      editedAt: '',
    };
    setMessages([...messages, newMessage]);
    setInput('');
  };

  const deleteMessage = (id) => {
    const newMessages = [...messages];
    newMessages.splice(
      newMessages.findIndex((m) => {
        return m.id === id;
      }),
      1
    );
    setMessages(newMessages);
  };

  const editMessage = (id) => {
    const newMessages = [...messages];
    newMessages[
      newMessages.findIndex((m) => {
        return m.id === id;
      })
    ].text = input;
    setMessages(newMessages);
  };

  const content = (
    <div>
      <Header info={messages} />
      <MessageList
        messages={messages}
        onDelete={deleteMessage}
        onEdit={editMessage}
      />
      <Form input={input} setInput={setInput} handleSubmit={handleSubmit} />
    </div>
  );

  return (
    <div className="chat" style={style}>
      {isLoaded ? content : <Preloader />}
    </div>
  );
};

const style = {
  backgroundColor: '#F4F1DE',
  padding: '50px 20vw',
};

export default Chat;
