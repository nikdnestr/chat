import React from 'react';

export const Form = ({ input, setInput, handleSubmit }) => {
  return (
    <form className="message-input" style={style.form} onSubmit={handleSubmit}>
      <div>
        <input
          className="message-input-text"
          style={style.input}
          value={input}
          onChange={(e) => setInput(e.target.value)}
          placeholder="To edit your message type something here and then press the edit button, yeah I know its stupid"
        />
        <input
          className="message-input-button"
          style={style.btn}
          type="submit"
          value="Send"
        />
      </div>
    </form>
  );
};

const style = {
  form: {
    padding: '20px 50px 0 50px',
  },
  input: {
    width: 'calc(100% - 55px)',
    boxSizing: 'border-box',
    padding: '5px',
    border: 'black 1px solid',
    borderRight: 'none',
    outline: 'none',
    background: '#fff',
  },
  btn: {
    background: '#fff',
    borderLeft: 'none',
    border: 'black 1px solid',
    outline: 'none',
    padding: '5px',
  },
};
