import React from 'react';
import HeaderItem from './HeaderItem';

export const Header = ({ info }) => {
  const time = new Date(info[info.length - 1].createdAt);
  const participants = info
    .map((m) => m.user)
    .filter((value, index, self) => self.indexOf(value) === index);

  const makeDate = (date) => {
    let d = date.getDate();
    let m = date.getMonth() + 1;
    if (d < 10) {
      d = `0${d}`;
    }
    if (m < 10) {
      m = `0${m}`;
    }
    return `${d}.${m}.${date.getFullYear()}`;
  };

  return (
    <header className="header" style={style}>
      <HeaderItem text="My chat" className={'header-title'} />
      <HeaderItem
        text={`${participants.length}`}
        className={'header-users-count'}
      />
      <HeaderItem text={info.length} className={'header-messages-count'} />
      <HeaderItem
        text={`${makeDate(time)} ${time.toLocaleTimeString('ru-Ru', {
          timeStyle: 'short',
        })}`}
        className={'header-last-message-date'}
        position="auto"
      />
    </header>
  );
};

const style = {
  border: 'black 1px solid',
  backgroundColor: '#F2CC8F',
  display: 'flex',
  flexDirection: 'row',
};
