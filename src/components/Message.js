import React, { useState } from 'react';
import { AiFillHeart, AiFillDelete, AiFillEdit } from 'react-icons/ai';
import { Avatar } from './Avatar';

export const Message = ({ message, onDelete, onEdit }) => {
  const [liked, setLiked] = useState(false);

  const time = new Date(message.createdAt).toLocaleTimeString('ru-Ru', {
    timeStyle: 'short',
  });

  const onLike = () => {
    liked ? setLiked(false) : setLiked(true);
  };

  const checkLike = () => {
    return liked ? style.helper.red : style.helper.black;
  };

  const ownMsg = (
    <div className="own-message" style={style.ownMessage.container}>
      <div
        className="message-delete"
        style={style.ownMessage.icon}
        onClick={() => onDelete(message.id)}
      >
        <AiFillDelete />
      </div>
      <div
        className="message-edit"
        style={style.ownMessage.icon}
        onClick={() => onEdit(message.id)}
      >
        <AiFillEdit />
      </div>
      <div style={style.ownMessage.textContainer}>
        <div className="message-text">{message.text}</div>
        <div className="message-time">{time}</div>
      </div>
    </div>
  );

  const msg = (
    <div className="message" style={style.message.container}>
      <Avatar className="message-user-avatar" src={message.avatar} />
      <div style={style.message.textContainer}>
        <div className="message-user-name" style={style.message.name}>
          {message.user}
        </div>
        <div className="message-text">{message.text}</div>
        <div className="message-time">{time}</div>
      </div>
      <div
        className={liked ? 'message-liked' : 'message-like'}
        style={{ ...style.message.like, ...checkLike() }}
        onClick={() => onLike()}
      >
        <AiFillHeart />
      </div>
    </div>
  );

  return message.user === 'Nik' ? ownMsg : msg;
};

const style = {
  ownMessage: {
    container: {
      display: 'flex',
      color: '#000',
      backgroundColor: 'gray',
      border: 'black 1px solid',
      borderRight: 'none',
      maxWidth: '35vw',
      minHeight: '100px',
      margin: '10px 0 10px auto',
    },
    textContainer: { margin: '15px 10px 0 auto' },
    name: {
      fontSize: '14px',
      fontWeight: 'bold',
      float: 'right',
      margin: '10px 0 5px',
    },
    icon: { fontSize: '25px', margin: 'auto 0 0 4px' },
  },
  message: {
    container: {
      display: 'flex',
      color: '#000',
      backgroundColor: '#C0D8CC',
      border: 'black 1px solid',
      borderLeft: 'none',
      maxWidth: '35vw',
      minHeight: '20px',
      margin: '10px 0',
    },
    textContainer: { marginLeft: '10px' },
    name: {
      fontSize: '14px',
      fontWeight: 'bold',
      margin: '10px 0 5px',
    },
    like: {
      fontSize: '25px',
      margin: 'auto 7px 0 auto',
      float: 'right',
    },
  },
  helper: {
    red: { color: 'red' },
    black: { color: 'black' },
  },
};
