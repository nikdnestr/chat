import React from 'react';
import { Message } from './Message';

export const MessageList = ({ messages, onDelete, onEdit }) => {
  const msgArr = messages.map((msg) => {
    return (
      <Message message={msg} onDelete={onDelete} onEdit={onEdit} key={msg.id} />
    );
  });

  return (
    <div className="message-list" style={style}>
      {msgArr}
    </div>
  );
};

const style = {
  marginTop: '10px',
  minHeight: '80vh',
  border: 'black 1px solid',
};
